var words = {
    "Red": [
        "a",
        "all",
        "and",
        "are",
        "at",
        "but",
        "can",
        "for",
        "have",
        "I",
        "in",
        "is",
        "it",
        "me"
    ],
    "2s": [
        "2 x 9",
        "9 x 2",
        "2 x 8",
        "8 x 2",
        "2 x 7",
        "7 x 2"
    ]
};

var gameState = {};

function correct() {
    gameState.correct.push(gameState.wordList.shift());
    nextWord();
}

function incorrect() {
    gameState.incorrect.push(gameState.wordList.shift());
    nextWord();
}

function gameOver() {
    let content = "";
    let total = gameState.correct.length + gameState.incorrect.length;
    let percentage = Math.floor(100 * gameState.correct.length / total);
    content += "<h1>" + percentage + "%</h1>";
    if (gameState.incorrect.length > 0) {
        content += "<h3>Incorrect Words:</h3>";
        content += "<ul class=\"list-group\">";
        for (var index in gameState.incorrect) {
            var word = gameState.incorrect[index];
            content += "<li class=\"list-group-item\">" + word + "</li>";
        }
        content += "</ul>";
    }
    content += "<div class=\"btn-group\">";
    content += "<button type=\"button\" class=\"btn btn-primary\" onclick=\"main()\">Restart!</button>";
    content += "</div>";
    $("#root").html(content);

}

function nextWord() {
    if (gameState.wordList.length === 0) {
        gameOver();
    } else {
        let content = "";
        content += "<div class=\"jumbotron\">";
        content += "<h1>" + gameState.wordList[0] + "</h1>";
        content += "</div>";
        content += "<p>Items Left: " + (gameState.wordList.length - 1) + "</p>";
        content += "<div class=\"btn-group\">";
        content += "<button type=\"button\" class=\"btn btn-success\" onclick=\"correct()\">Correct!</button>";
        content += "<button type=\"button\" class=\"btn btn-danger\" onclick=\"incorrect()\">Incorrect!</button>";
        content += "</div>";
        $("#root").html(content);
    }
}

function startSession(key) {
    gameState.wordList = words[key].slice(0);
    gameState.wordList.sort((a, b) => (0.5 - Math.random()));
    gameState.correct = [];
    gameState.incorrect = [];
    nextWord();
}

function main() {
    let content = "";
    content += "<div class=\"list-group\">";
    for (var key in words) {
        content += "<a class=\"list-group-item\" onclick=\"startSession('" + key + "')\">" + key + "</a>";
    }
    content += "</div>";
    $("#root").html(content);
}

main();